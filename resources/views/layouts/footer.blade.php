<!-- Footer de la web --> 

<footer class="footer text-faded text-center py-5">
    <div class="container">
		<div class="zerogrid wrapper">
			Copyright @ QARMITA - Diseñado por <a>Daniel &amp; Julio </a>
			<ul class="quick-link">
                <a href="#">Política de privacidad</a>
                <br>
				<a href="#">Términos de uso</a>
			</ul>
		</div>
	</div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>